<main class="container">
<div class="row">
    <div class="col  s12 l12">
        <h3>Dashboard</h3>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
    </div>
</div>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content orange">
                <div class="card-title white-text">
                    <h3 style=" margin: 10px !important;">
                        WELCOME, <?=$userinfo->acc_firstname." ".$userinfo->acc_lastname?>!
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s4">
        <div class="card">
            <div class="card-content black orange-text">
                <h5 style="margin: 0px !important;">       
                    JCOIN Points 
                </h5>
            </div>
            <div class="card-action grey darken-4 orange-text">

                <i class="material-icons left ">monetization_on</i> <?php if($points[0]->points==0){echo 0;}else{echo$points[0]->points;} ?>
            </div>
        </div>
        <div class="card" style="overflow-y: scroll; max-height:500px;">
            <div class="card-content small">
                <span class="card-title">
                    <h4 style="margin: 0px !important;" class="orange-text darken-4">
                        Announcements
                    </h4>
                    <div class="divider orange"></div>
                    <div class="divider orange"></div>
                </span>
                <?php foreach($announcements as $a): ?>
                <blockquote class="grey lighten-4">
                    <h5 style="margin: 0px !important;"><?=$a->ann_title?></h5>
                    <i class="grey-text">Posted: <?= date("F j, Y", strtotime($a->ann_updated_at)) ?></i>
                    <p><?=$a->ann_content?></p>
                </blockquote>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col s8">

        <div class="card sticky-action" style="overflow-y: scroll; max-height:650px;">
            <div class="card-content small">
                <span class="card-title">
                    <h4 style="margin: 0px !important;" class="orange-text darken-4">
                        Events
                    </h4>
                    <div class="divider orange"></div>
                    <div class="divider orange"></div>
                </span>

                <?php foreach($events as $e): ?>
                 <?php if(!empty($e->event_img_path)){ ?>
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light white">
                        <div class="activator" align="center">
                            <img src="<?= base_url()?>images/events/<?=$e->event_img_path?>"  style="width:300px;">
                        </div>
                        <span class="card-title white-text activator" style="background-color:rgba(255,165,0,0.7);">
                            <b><?=$e->event_name?></b>
                        </span>
                    </div>
                    <div class="card-reveal orange">
                        <span class="card-title white-text"><b><?=$e->event_name?></b><i class="material-icons right">close</i></span>
                        <p class="white-text"><?=$e->event_description?></p>
                        <br>
                        <table class="orange darken-3 centered white-text">
                            <tr>
                                <tr>
                                    <td><b><?= date("F j, Y",$e->start) ?></b></td>
                                </tr>
                                <tr>
                                    <td class="white"><b class="orange-text darken-4"><?=date("H:i:s", $e->start)?></b></td>
                                </tr>
                                <tr>    
                                    <?php if(!empty($e->event_venue)){?>
                                    <td><b><?=$e->event_venue?></b></td>
                                    <?php }else {?>
                                    <td><b>TBA</b></td>
                                    <?php } ?>
                                </tr>
                                </tr>
                        </table>
                    </div>
                    <div class="card-action center-align orange darken-4">
                        <a href="#!" class="white-text"><u><b>Register Now</b></u></a>
                    </div>
                </div>

                <?php }
                    else{ ?>
                
                <div class="card horizontal small">
                    <div class="card-image">
                        <img  src="" width="100%"  height="100%">
                    </div>
                    <div class="card-stacked">
                        <div class="card-content orange white-text ">
                            <span class="card-title">
                                <h4 style="margin: 0px !important;" class="white-text center-align"><?=$e->event_name?></h4>
                            </span>
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col s12 ">
                                    <ul class="collection">
                                        <li class="collection-item orange darken-4"><b>Date <span class="secondary-content white-text"><?= date("F j, Y", $e->start) ?></span></b></li>
                                        <li class="collection-item orange darken-3"><b>Time <span class="secondary-content white-text"><?=date("H:i:s", $e->start)?></span></b></li>
                                        <?php if(!empty($e->event_venue)){?>
                                        <li class="collection-item orange darken-4"><b>Venue <span class="secondary-content white-text">TBA</span></b></li>
                                        <?php } else{ ?>
                                         <li class="collection-item orange darken-4"><b>Venue <span class="secondary-content white-text"><?=$e->event_venue?></span></b></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-action grey lighten-4 center-align">
                            <a href="#!" class="orange-text"><u><b>Register Now</b></u></a>
                        </div>
                    </div>
                </div>
                
                    <?php } ?>

                <?php endforeach; ?>
                
            </div>
        </div>


    </div>
</div>
