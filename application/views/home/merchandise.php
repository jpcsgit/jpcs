<main class="container">
<div class="row">
    <div class="col  s12 l12">
        <h3>Merchandise</h3>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
    </div>
</div>
<div class="row">

    <?php foreach($merchandise as $m):?>
    <div class="col s4">
        <div class="card orange darken-4">
            <div class="card-image waves-effect waves-block waves-light white">
                <?php if(!$m->merch_img_path) { ?>
                <img class="activator" src="<?= base_url() ?>images/merchandise/default.jpg" style="width:100%; max-width:400px;">
                <?php } else {?>
                <img class="activator" src="<?= base_url() ?>images/merchandise/<?=$m->merch_img_path?>"  style="width:100%; max-width:400px;">
                <?php } ?>                
            </div>
            <div class="card-content">
                <span class="card-title activator white-text center-align"><b><?=$m->merch_name?></b></span>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4"><?=$m->merch_name?><i class="material-icons right">close</i></span>
                <p>
                    <?=$m->merch_content?>
                </p>
                <p><b>Price : Php <?=$m->merch_price?></b></p>
                <a class="btn orange white-text waves-effect" data-toggle="modal" data-target="#ordermodal" >Order Now</a>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>
<br>
</main>
<div class="modal fade" style="height:600px;" id="ordermodal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Event Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form" action="<?=base_url()?>admin/add_event" id="add_event" method="post" enctype="multipart/form-data">
                <div class="form-group">
                <label for="startdate">Event Title</label>
                    <input class="form-control" id="atitle" type="text" name="title">
                </div>
                <div class="form-group">
                <label for="startdate">Description</label>
                    <textarea class="form-control" id="acontent" type="text" name="content" rows="5" maxlength="200"> </textarea>
                </div>
                <div class="form-group">
                <label for="startdate">Venue</label>
                    <input class="form-control" id="venue" type="text" name="venue">
                </div>
                 <div class="form-group">   
                <label for="startdate">Event Image</label>      
                    <input class="form-control" type="file" id="userfile" name="userfile">
                </div> 
                <div class="form-group">
                <label for="startdate">Start Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="astartdate"  min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="startdate">
                    <input class="form-horizontal" id="starttime" type="time" name="starttime">
                    </div>
                </div>
                <div class="form-group">
                <label for="startdate">End Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="aenddate" min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="enddate">
                    <input class="form-horizontal" id="endtime" type="time" name="endtime">
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" id="create">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>           
            </div>
        </div>
    </div>
</div>   