    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('<?=base_url()?>assets/img/home-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1 style="color:#ff8d02;">Welcome To JPCS-FEUTECH Portal</h1>
                        <hr class="small">
                        <span class="subheading"></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <?php foreach($announcement as $a): ?>
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            <?=$a->ann_title?>
                        </h2>
                        <h3 class="post-subtitle">
                            <?=$a->ann_content?>
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">JPCS FEUTECH</a> on September 24, 2014</p>
                </div>
                <hr>
            <?php endforeach; ?>
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr>

   