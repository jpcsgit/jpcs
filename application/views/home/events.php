 <main class="container">
<div class="row">
    <div class="col  s12 l12">
        <h3>Upcoming and Current Events</h3>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
        <div class="divider orange"></div>
    </div>
</div>
<?php foreach($events as $e): ?>
                 <?php if(!empty($e->event_img_path)){ ?>
<div class="row">
    <div class="col  s12 l12">
        <div class="card horizontal hoverable medium">
            <div class="card-image">
                <img  src="<?= base_url() ?>images/events/<?=$e->event_img_path?>" style="width:400px;">
                <span class="card-title"><h2 class="white-text"><?=$e->event_name?></h2></span>
            </div>
            <div class="card-stacked">
                <div class="card-content white">
                    <div class="row">
                        <div class="col s12">
                            <h4 class="orange-text center-align"><b><?=$e->event_name?></b>
                                <div class="divider"></div></h4>
                            <?=$e->event_description?>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col s12">
                            <table class="orange centered">
                                <tr>
                                <tr>
                                    <td><b><?= date("F j, Y",$e->start) ?></b></td>
                                </tr>
                                <tr>
                                    <td class="white"><b class="orange-text darken-4"><?=date("H:i:s", $e->start)?></b></td>
                                </tr>
                                <tr>    
                                    <?php if(!empty($e->event_venue)){?>
                                    <td><b><?=$e->event_venue?></b></td>
                                    <?php }else {?>
                                    <td><b>TBA</b></td>
                                    <?php } ?>
                                </tr>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-action orange darken-3 center-align">
                    <a href="<?= base_url() ?>JPCS/eventRegistration" class="white-text"><u><b>Register Now</b></u></a>
                </div>
            </div>
        </div>
    </div>
</div>
                    <?php }
                    else{ ?>

<div class="row">
    <div class="col  s12 l12">
        <div class="card horizontal hoverable medium">
            <div class="card-image">
                <span class="card-title"><h2 class="  white-text"><?=$e->event_name?></h2></span>
            </div>
            <div class="card-stacked">
                <div class="card-content orange">
                    <div class="row">
                        <div class="col s12">
                            <h4 class="text center-align"><b><?=$e->event_name?></b>
                                <div class="divider"></div></h4>
                            <?=$e->event_description?>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col s12">
                            <table class="orange centered">
                                <tr>
                                <tr>
                                    <td><b><?= date("F j, Y",$e->start) ?></b></td>
                                </tr>
                                <tr>
                                    <td class="white"><b class="orange-text darken-4"><?=date("H:i:s", $e->start)?></b></td>
                                </tr>
                                <tr>    
                                    <?php if(!empty($e->event_venue)){?>
                                    <td><b><?=$e->event_venue?></b></td>
                                    <?php }else {?>
                                    <td><b>TBA</b></td>
                                    <?php } ?>
                                </tr>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-action orange darken-3 center-align">
                    <a href="<?= base_url() ?>JPCS/eventRegistration" class="white-text"><u><b>Register Now</b></u></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

                <?php endforeach; ?>
</main>