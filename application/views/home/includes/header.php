<!DOCTYPE html>
<html>
    <head>
        <title><?=$title?></title>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url().'assets/img/logo.png'?>">
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/css/materialize.min.css" media="screen,projection"/>
        <script src="<?= base_url() ?>assets/js/jquery-3.3.1.min.js"></script>  
        <script type="text/javascript" src="<?= base_url() ?>assets/js/materialize.min.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>assets/iconfont/material-icons.css" >
        <style>
            nav ul li.active {
                background-color: rgb(195, 105, 0) !important;
            }
            nav ul a:hover {
                background-color: rgb(255, 165, 0) !important;
            }
            html {
                background-image: linear-gradient(white,white, orange);
                background-attachment: fixed;
            }
            blockquote {
                border-left: 5px solid orange;
            }
        </style>
    </head>

    <body >
       
        


