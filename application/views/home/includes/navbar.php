     <header>
            <nav class="black">
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo left">&nbsp;&nbsp;JPCS FEU Tech</a>
                    <ul class="right">
                        <li <?= ($this->uri->segment(1) == "home") ? "class='active'" : "" ?>>
                            <a href="<?= base_url()?>home/">
                                <i class="material-icons left"> dashboard</i>
                                Dashboard
                            </a>
                        </li>
                        <li <?= ($this->uri->segment(1) == "events" || $this->uri->segment(2) ==  'eventRegistration') ? "class='active'" : "" ?>>
                            <a href="<?= base_url() ?>events">
                                <i class="material-icons left" > event</i>
                                Events
                            </a>
                        </li>
                        <li <?= ($this->uri->segment(1) == "merchandise") ? "class='active'" : "" ?>>
                            <a href="<?= base_url() ?>merchandise">
                                <i class="material-icons left"> card_giftcard</i>
                                Merchandise
                            </a>
                        </li>
                        <li>
                            <a>
                                <i class="material-icons left"> note</i>
                                Raffle
                            </a>
                        </li>
                        <li <?= ($this->uri->segment(1) == "aboutUs") ? "class='active'" : "" ?>>
                            <a href="<?= base_url() ?>JPCS/aboutUs">
                                <i class="material-icons left"> info</i>
                                About Us
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>home/logout">
                                <i class="material-icons left"> exit_to_app</i>
                                Log Out
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>