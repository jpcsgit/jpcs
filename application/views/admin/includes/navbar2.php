    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">

      <a class="navbar-brand" href="<?=base_url()?>admin/">

        <img src="<?=base_url()?>assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">

         &nbsp;JPCS FEU Tech

      </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">

        <span class="navbar-toggler-icon"></span>

      </button>

      <div class="collapse navbar-collapse" id="navbarNav">

        <ul class="navbar-nav">

          <li class="nav-item">

            <a class="nav-link" href="<?=base_url()?>admin/">Dashboard</a>

          </li>

          <li class="nav-item">

            <a class="nav-link" href="<?=base_url()?>admin/events">Events</a>

          </li>

          <li class="nav-item dropdown">

            <a class="nav-link dropdown-toggle" href="#" id="databaseDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Database</a>

            <div class="dropdown-menu" aria-labelledby="databaseDropdown">

              <a class="dropdown-item" href="<?=base_url()?>members">Members</a>
              <a class="dropdown-item" href="<?=base_url()?>accounts">Accounts</a>
              <a class="dropdown-item" href="transactionsdb.php">Transactions</a>

            </div>

          </li>

          <li class="nav-item">

            <a class="nav-link" href="registration.php">Registration</span></a>

          </li>

          <li class="nav-item">

            <a class="nav-link" href="merchandise.php">Merchandise</a>

          </li>

          <li class="nav-item">

            <a class="nav-link" href="raffle.php">Raffle</a>

          </li>

          <li class="nav-item">

            <a class="nav-link nav-right" href="<?=base_url()?>admin/logout">Log Out</a>

          </li>



        </ul>

      </div>

    </nav>