<div class="wrapper">
<!-- Overlay for fixed sidebar -->
<div class="sidebar-overlay"></div>

<!-- Material sidebar -->
<aside id="sidebar" class="sidebar sidebar-default open" role="navigation">
    <!-- Sidebar header -->
    <div class="sidebar-header header-cover" style="background-image: url(<?=base_url()?>assets/img/home-bg.jpg); background-size: 300px;">
        <!-- Top bar -->
        <div class="top-bar"></div>
        <!-- Sidebar toggle button -->
        <button type="button" class="sidebar-toggle">
            <i class="icon-material-sidebar-arrow"></i>
        </button>
        <!-- Sidebar brand image -->
        <div class="sidebar-image">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/53474/atom_profile_01.jpg">
        </div>
        <!-- Sidebar brand name -->
        <a data-toggle="dropdown" class="sidebar-brand"  href="#settings-dropdown">
           <strong style="text-color:white;"><?= $admin->firstname." ".$admin->lastname ?></strong>
           
        </a>
    </div>

    <!-- Sidebar navigation -->
    <ul class="sidebar-nav">
       
        <li>
            <a href="<?= base_url().'admin/index'?>">
                <i class="sidebar-icon md-inbox"></i>
                Dashboard
            </a>
        </li>

        <li>
            <a href="<?= base_url().'members'?>">
                <i class="sidebar-icon md-drafts"></i>
                Members 
            </a>
        </li>

          <li>
            <a href="<?= base_url().'admin/announcements'?>">
                <i class="sidebar-icon md-drafts"></i>
                Announcements
            </a>
        </li>

        <li>
            <a href="<?= base_url().'admin/events'?>">
                <i class="sidebar-icon md-drafts"></i>
                Events
            </a>
        </li>

        <li>
            <a href="<?= base_url().'admin/merchandise'?>">
                <i class="sidebar-icon md-drafts"></i>
                Merchandise
            </a>
        </li>
     
         <li>
            <a href="<?= base_url().'admin/viewTransactions'?>">
                <i class="sidebar-icon md-drafts"></i>
                Transactions
            </a>
        </li>
        <li>
            <a href="<?= base_url().'accounts'?>">
                <i class="sidebar-icon md-drafts"></i>
                Accounts
            </a>
        </li>
   
        <li>
           <a href="<?= base_url().'admin/logout'?>">
                Log Out
            </a>
        </li>
    </ul>
    <!-- Sidebar divider -->
    <!-- <div class="sidebar-divider"></div> -->
    
    <!-- Sidebar text -->
    <!--  <div class="sidebar-text">Text</div> -->
</aside>
</div>