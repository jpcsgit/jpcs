 <div class="wrapper" style="width:1200px"> 
    <!-- Sidebar Constructor -->
    <div class="constructor">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow p-5 mb-5 bg-white rounded" style="margin-top: 50px; padding: 150px 0px 100px 20px;">
                    <div class="card-body">
                        <h1 class="card-title animated fadeIn display-1" style="font-size: 72px;">Welcome, <?=$userinfo->firstname?> <?=$userinfo->lastname ?></h1>
                    </div>
                    <?=$userinfo->firstname?> <?=$userinfo->lastname ?> 
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-12"> -->
                <div class="col-md-6">
                    <div class="card shadow p-3 mb-5 rounded" style="height:200px; margin-top: 5px; animation-delay: 0.5s; background-color: #ff9400;">
                        <div class="card-body">
                            <h3 class="card-title" align="center" style="font-size: 48px;"><i class="material-icons" style="font-size:40px">people</i>&nbsp; <?= $members ?> </h3>
                            <p class="card-text lead" align="center" style="font-size: 24px;">Number of Registered Members.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card shadow p-3 mb-5 rounded" style="height:200px; margin-top: 5px; animation-delay: 0.5s; background-color: #ff9400;">
                        <div class="card-body">
                            <h3 class="card-title" align="center" style="font-size: 48px;"><i class="material-icons" style="font-size:40px">people</i>&nbsp; <?= $events ?> </h3>
                            <p class="card-text lead" align="center" style="font-size: 24px;">Upcoming events.</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
 </div> 
