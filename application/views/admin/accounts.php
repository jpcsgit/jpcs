<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-3 mb-5 bg-white rounded animated slideInUp" style="margin-top: 30px;">
                <div class="card-body">
                    <h1 class="card-title display-4"><i class="material-icons" style="font-size:48px;">list</i>&nbsp;Accounts</h1>
                    <p class="card-text">Members database gives you quick access to manage members that let you safeguard organization's data, privacy, and decide how your information can make services work better for us.</p>
                    <hr>
                    <a class="card-text" href="registration.php"><i class="material-icons" style="font-size:18px">add_circle_outline</i>&nbsp;Add a New Member</a>
                    <!-- <form method="POST">
                        <div class="form-inline" style="float: right; clear: both;">
                            <input type="text" class="form-control" id="search" aria-describedby="searchHelp" name="search" placeholder="Search Student Number">&nbsp;&nbsp;
                            <button class="btn btn-outline-success my-2 my-sm-0" name="btnSubmit" type="submit">Search</button>
                        </div>
                    </form> -->
                    <br/><br/>
                    <table class="table table-hover table-responsive-md" id="accountsTable">
                        <!--<caption>Legends: Admin - Can View, Modify, and Delete Data to every features of this system. | Editor - Can View, Modify, and Delete Data to Members Table only. | Registration - Can Add Data to Members Table only.</caption>-->
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Student Number</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">M.I</th>
                                <th scope="col">Status</th>
                                <!-- <th scope="col">Year Level</th> -->
                                <!-- <th scope="col">Course</th>
                                <th scope="col">Gender</th> -->
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($members as $m): ?>
                                <tr>
                                    <th scope="row"><?= $m->student_number ?></th>
                                    <td><?= $m->acc_firstname ?></td>
                                    <td><?= $m->acc_lastname ?></td>
                                    <td><?= $m->acc_middlename ?></td>
                                    <td><?= $m->acc_status ?></td>
                                    <!-- <td><?= $m->course ?></td>
                                    <td><?= $m->gender ?></td> -->
                                    <td><button type="button" class="btn btn-primary" data-backdrop="false" data-toggle="modal" data-target="#<?=$m->student_number?>">
                                            <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button>
                                    <button type="button" onclick="window.location.href = '<?=base_url()?>accounts/edit?studnum=<?=$m->student_number?>';" class="btn btn-success">
                                    <i class="material-icons" style="font-size:18px">create</i>&nbsp;Edit</button> 
                                    </td>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
$(document).ready( function () {
$('#accountsTable').DataTable();
} );
</script>

 <!-- <div class="modal fade" id="<?= $m->student_no ?>" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Member's Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Student Number: <?= $m->student_number?></p>
                        <h2><?= $m->acc_firstname  ?> <?= $m->acc_middlename?> <?= $m->acc_lastname?></h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <p><strong>Home Address:</strong> <?= $m->address?><br/>
                            <strong>Birthdate:</strong> <?= date("F j, Y A",$m->birthday)?><br/>
                            <strong>Gender:</strong> <?= $m->gender?></p>
                    </div>
                    <div class="col-md-5">
                        <p><strong>Year Level:</strong> <?= $m->year_level?><br/>
                            <strong>Course:</strong> <?= $m->course?></p>
                    </div>
                </div> 
                    <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <p><strong>Contact No.:</strong> <?= $m->contact_no ?><br/>
                            <strong>Email Address:</strong> <?=$m->email?><br/>
                            <strong>Facebook Profile:</strong> <a href="<?= $m->fb_link ?>" target="_blank"><?=$m->fb_link?></a></p>
                    </div>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>
        
            </div>
        </div>
    </div>
</div> -->
