  <html>
  <head>
  <title></title>
    
      <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url().'assets/img/logo.png'?>">

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  </head>
  <body background="<?=base_url()?>assets/img/bg3.jpg" style="overflow: hidden; height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover;">
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <img class="animated fadeIn" src="<?=base_url()?>assets/img/logo.png" align="center" style="margin-top: 50px; display: block; margin-left: auto; margin-right: auto; width: 30%; animation-delay: 1s;">
          <div class="card shadow-lg p-3 mb-5 bg-white rounded animated slideInUp" style="width: 31rem; margin-top: 50px;">
            <div class="card-body">
              <h5 class="card-title">JPCS Organization Portal</h5>
              <p class="card-text">Welcome to our JPCS Organization Portal! To access our system you must login below.</p>
              <p class="card-text" style="font-size: 12px;">Dont have an account? <a href="#">Request an Account</a>.</p>
              <form method="POST" action="<?=base_url()?>admin/login">
                <div class="form-group">
                  <label for="user">Student Number</label>
                  <input type="text" class="form-control" id="user" aria-describedby="userHelp" name="studnum" placeholder="Enter Username">
                </div>
                <div class="form-group">
                  <label for="pass">Password</label>
                  <input type="password" class="form-control" id="pass" aria-describedby="emailHelp" name="password" placeholder="Enter Password">
                </div> 
                <div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="rememberMe">
                  <label class="form-check-label" for="rememberMe">Remember Me</label>
                </div><br/>
                <button type="submit" name="btnSubmit" class="btn btn-primary btn-lg btn-block">Login</button>
              </form>
              
            </div>
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>  
      <footer></footer>
        
        <div class="row">
          <hr>
          <div class="col-md-12">
            <br/>
            <p class="animated fadeIn" align="center" style="animation-delay: 2s; color: white;"><strong>Created by:</strong> Toby Villareal &copy; All Rights Reserved 2018.</p>
          </div>
        </div>
      </footer>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://raw.githubusercontent.com/HubSpot/odometer/v0.4.6/odometer.min.js"type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>