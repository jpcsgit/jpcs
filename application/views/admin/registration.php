
  <div style="background-image:url('<?=base_url()?>assets/img/bg3.jpg'); overflow: hidden; background-position: center; background-repeat: no-repeat; background-size: cover;">
    <div class="container">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <img class="animated fadeIn" src="<?=base_url()?>assets/img/logo.png" align="center" style="margin-top: 50px; display: block; margin-left: auto; margin-right: auto; width: 30%; animation-delay: 1s;">
          <div class="card shadow-lg p-3 mb-5 bg-white rounded animated slideInUp" style="width: 31rem; margin-top: 50px;">
            <div class="card-body">
              <h5 class="card-title">JPCS Organization Portal</h5>
              <p class="card-text">Welcome to our JPCS Organization Portal! To access our system you must login below.</p>
              <p class="card-text" style="font-size: 12px;">Dont have an account? <a href="#">Request an Account</a>.</p>
              <form method="POST" action="<?=base_url()?>admin/login">
                <div class="form-group">
                  <label for="user">Student Number</label>
                  <input type="text" class="form-control" id="user" aria-describedby="userHelp" name="studnum" placeholder="Enter Username">
                </div>
                <div class="form-group">
                  <label for="pass">Password</label>
                  <input type="password" class="form-control" id="pass" aria-describedby="emailHelp" name="password" placeholder="Enter Password">
                </div> 
                <div class="form-group">
                  <label for="pass">Confirm Password</label>
                  <input type="password" class="form-control" id="con" aria-describedby="emailHelp" name="conpassword" placeholder="Confirm Password">
                </div> 
                <button type="submit" name="btnSubmit" class="btn btn-primary btn-lg btn-block">Register</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-3"></div>
      </div>
      <footer></footer>
        <div class="row">
          <hr>
          <div class="col-md-12">
            <br/>
            <p class="animated fadeIn" align="center" style="animation-delay: 2s; color: white;"><strong>Created by:</strong> Deomar Torres, Toby Villareal &copy; All Rights Reserved 2018.</p>
          </div>
        </div>
      </footer>
    </div>
</div>