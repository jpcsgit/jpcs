 <table class="table table-hover table-responsive-md" id="mertbl">
    <!--<caption>Legends: Admin - Can View, Modify, and Delete Data to every features of this system. | Editor - Can View, Modify, and Delete Data to Members Table only. | Registration - Can Add Data to Members Table only.</caption>-->
    <thead class="thead-light">
        <tr>
            <th scope="col">Image</th>    
            <th scope="col">Name</th>
            <th scope="col" style="width:100px;">Content</th>
            <th scope="col">Price</th>
            <th scope="col">Date Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($merchandise as $m):?>
            <tr>
            <?php $cont = preg_replace( "/\r|\n/", " ", $m->merch_content ) ?>
            <td><img src="<?=base_url()?>images/merchandise/<?=$m->merch_img_path ?>" style="width:100px;"></td>
                <td><?=$m->merch_name?></td>
                <td><?=$m->merch_content ?></td>
                <td><?=$m->merch_price ?></td>
                <td><?=$m->merch_created_at ?></td>
                <td>
                    <button type="button" class="btn btn-primary btn-block" data-backdrop="false" 
                    data-toggle="modal" data-target="#viewModal"
                    onclick="viewModal(<?= $m->merch_id  ?>,'<?= $m->merch_name  ?>','<?=$m->merch_content?>','<?=$m->merch_price ?>','<?=$m->merch_img_path ?>')">
                    <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button>

                     <a type="button" class="btn btn-danger btn-block" data-backdrop="false" href="<?=base_url()?>admin/remove_merch/<?=$m->merch_id ?>">
                     Remove</a>

                </td>                                 
            </tr>
                                    
    <?php endforeach; ?>
    </tbody>
</table>
<script>
// $(document).ready(function(){
//     $('#eventview').click(function(){
//         alert($(this).data("id"));
//         $("#ann_id").val($(this).data("id"));
//         $("#title").val($(this).data("name"));
//         $("#content").val($(this).data("content"));
//         $("#enddate").val($(this).data("enddate"));
//         $("#endtime").val($(this).data("endtime"));
//         console.log($(this).data("id"))
//         // $("#title").val($(this).data("end"));
//     });
// });

function viewModal(id,title,desc,price,img){
    $("#merch_id").val(id);
    $("#title").val(title);
    $("#content").val(desc);
    $("#price").val(price);
    $("#img").attr('src',"<?=base_url()?>images/merchandise/"+img);
}
$(document).ready( function () {
    $('#mertbl').DataTable({
        "order": [],
    "scrollY":"500px",
    "scrollCollapse": true,
    });
});
</script>