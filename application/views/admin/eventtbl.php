 <table class="table table-hover" id="evtbl">
    <!--<caption>Legends: Admin - Can View, Modify, and Delete Data to every features of this system. | Editor - Can View, Modify, and Delete Data to Members Table only. | Registration - Can Add Data to Members Table only.</caption>-->
    <thead class="thead-light">
        <tr>
            <th scope="col">Event Title</th>
            <th scope="col">Event Description</th>
            <th scope="col">Date Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($events as $e):?>
            <tr>
            <?php $desc = preg_replace( "/\r|\n/", " ", $e->event_description ) ?>
                <td><?=$e->event_name?></td>
                <td><?=$e->event_description?></td>
                 <td><?= date("Y-M-d H:i:s",strtotime($e->event_created_at)) ?></td>
                <td>
                     <button type="button" class="btn btn-primary btn-block" data-backdrop="false" 
                    data-toggle="modal" data-target="#viewModal"
                    onclick="viewModal(<?= $e->event_id ?>,'<?= $e->event_name ?>','<?=$desc?>','<?= $e->event_venue ?>','<?= date("Y-m-d",$e->start)?>','<?= date("H:i:s",$e->start)?>',
                    '<?= date("Y-m-d",$e->end)?>','<?= date("H:i:s",$e->end)?>','<?= $e->event_img_path ?>')">
                        <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button> 
                    <!-- <button type="button" class="btn btn-primary btn-block" data-backdrop="false" id="eventview"
                    data-toggle="modal" data-target="#viewModal" 
                    data-id="<?= $e->event_id ?>"
                    data-name="<?= $e->event_name ?>"
                    data-content="<?= $e->event_description ?>"
                    data-venue="<?= $e->event_venue ?>"
                    data-startdate="<?= date("Y-m-d",$e->start)?>"
                    data-starttime="<?= date("H:i:s",$e->start)?>"
                    data-enddate="<?= date("Y-m-d",$e->end)?>"
                    data-endtime="<?= date("H:i:s",$e->end)?>"
                    >
                        <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button> -->
                    <a type="button" class="btn btn-warning btn-block" data-backdrop="false" href="<?=base_url()?>admin/stop_event/<?=$e->event_id?>">
                     Stop Event</a>

                     <a type="button" class="btn btn-danger btn-block" data-backdrop="false" href="<?=base_url()?>admin/cancel_event/<?=$e->event_id?>">
                     Cancel Event</a>

                </td>                                 
            </tr>
                                    
    <?php endforeach; ?>
    </tbody>
</table>
<script>
// $(document).ready(function(){
//     $('#eventview').click(function(){
//         $("#event_id").val($(this).data("id"));
//         $("#title").val($(this).data("name"));
//         $("#content").val($(this).data("content"));
//         $("#startdate").val($(this).data("startdate"));
//         $("#starttime").val($(this).data("starttime"));
//         $("#enddate").val($(this).data("enddate"));
//         $("#endtime").val($(this).data("endtime"));
        
//         // $("#title").val($(this).data("end"));
//     });
// });

function viewModal(id,title,desc,venue,sdate,stime,edate,etime,img){
    $("#event_id").val(id);
    $("#title").val(title);
    $("#content").val(desc);
    $("#venue").val(venue);
    $("#startdate").val(sdate);
    $("#starttime").val(stime);
    $("#enddate").val(edate);
    $("#endtime").val(etime);
    $("#img").attr('src',"<?=base_url()?>images/events/"+img);
}
$(document).ready( function () {
    $('#evtbl').DataTable({
        "order": [],
    "scrollY":"600px",
    "scrollCollapse": true,
    });
});
</script>