 <table class="table table-hover table-responsive-md" id="anntbl">
    <!--<caption>Legends: Admin - Can View, Modify, and Delete Data to every features of this system. | Editor - Can View, Modify, and Delete Data to Members Table only. | Registration - Can Add Data to Members Table only.</caption>-->
    <thead class="thead-light">
        <tr>
            <th scope="col">Announcement Title</th>
            <th scope="col" style="width:900px;">Content</th>
            <th scope="col">Date Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($announcements as $a):?>
            <tr>
            <?php $cont = preg_replace( "/\r|\n/", " ", $a->ann_content ) ?>
                <td><?=$a->ann_title?></td>
                <td><?=$a->ann_content?></td>
                 <td><?= date("Y-M-d H:i:s",strtotime($a->ann_created_at)) ?></td>
                <td>
                    <!-- <a type="button" class="btn btn-primary btn-block" data-backdrop="false" id="eventview"
                    data-toggle="modal" data-target="#viewModal" 
                    data-id="<?= $a->ann_id ?>"
                    data-name="<?= $a->ann_title ?>"
                    data-content="<?= $a->ann_content ?>"
                    data-enddate="<?= date("Y-m-d",$a->ann_end_date)?>"
                    data-endtime="<?= date("H:i:s",$a->ann_end_date)?>"
                    > -->
                    <button type="button" class="btn btn-primary" data-backdrop="false" 
                    data-toggle="modal" data-target="#viewModal"
                    onclick="viewModal(<?= $a->ann_id ?>,'<?= $a->ann_title ?>','<?=$cont?>',
                    '<?= date("Y-m-d",$a->ann_end_date)?>','<?= date("H:i:s",$a->ann_end_date)?>',)">
                    <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button>

                     <a type="button" class="btn btn-danger btn-block" data-backdrop="false" href="<?=base_url()?>admin/remove_ann/<?=$a->ann_id?>">
                     Remove</a>

                </td>                                 
            </tr>
                                    
    <?php endforeach; ?>
    </tbody>
</table>
<script>
// $(document).ready(function(){
//     $('#eventview').click(function(){
//         alert($(this).data("id"));
//         $("#ann_id").val($(this).data("id"));
//         $("#title").val($(this).data("name"));
//         $("#content").val($(this).data("content"));
//         $("#enddate").val($(this).data("enddate"));
//         $("#endtime").val($(this).data("endtime"));
//         console.log($(this).data("id"))
//         // $("#title").val($(this).data("end"));
//     });
// });

function viewModal(id,title,desc,edate,etime){
    $("#ann_id").val(id);
    $("#title").val(title);
    $("#content").val(desc);
    $("#enddate").val(edate);
    $("#endtime").val(etime);
}
$(document).ready( function () {
    $('#anntbl').DataTable({
        "order": [],
    "scrollY":"500px",
    "scrollCollapse": true,
    });
});
</script>