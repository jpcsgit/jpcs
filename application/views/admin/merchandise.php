<div class="wrapper">
    <!-- Sidebar Constructor -->
    <div class="constructor">
<!-- <div class="container"> -->

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-5 mb-5 bg-white rounded" style="margin-top: 30px; width:1200px;">
                <div class="card-body col-md-12">
                    <h1 class="card-title display-4"><i class="material-icons" style="font-size:48px;">list</i>&nbsp; Merchandise management</h1>
                    <!-- <p class="card-text">Members database gives you quick access to manage members that let you safeguard organization's data, privacy, and decide how your information can make services work better for us.</p> -->
                    <hr>
                    <br/><br/>
                    <button type="button" class="btn btn-warning" style="float:right;"
                    data-toggle="modal" data-target="#addModal" data-backdrop="false">
                        Add New Merchandise</button>
                        <hr>
                        </br>
                    <div id="merchtable">
                    </div>
                
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<div class="modal fade" style="height:600px;" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Announcement Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form" action="<?=base_url()?>admin/submit_edit_merch" id="edit_merch" method="post" enctype="multipart/form-data">
                <div id="">
                <input type="hidden" id="merch_id" name="id">
                </div>
                 <center>
                <img id = "img" width="300">
            </center>
                <div class="form-group">     
                <label for="position">Add Image</label>      
                    <input class="form-control" type="file" id="userfile" name="userfile">
                </div>     
                <div class="form-group">
                <label for="name">Merchandise Name</label>
                    <input class="form-control" id="title" type="text" name="title">
                </div>
                <div class="form-group">
                <label for="desc">Description</label>
                    <textarea class="form-control" id="content" type="text" name="content" rows="5" maxlength="100"> </textarea>
                </div>
                <div class="form-group">
                <label for="price">Price</label>
                    <input class="form-control" id="price" type="number" name="price" step=".1">
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" id="edit" >Edit</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>           
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" style="height:600px;" id="addModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Announcement Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form" action="<?=base_url()?>admin/add_merch" id="add_merch" method="post"  enctype="multipart/form-data">
                <div class="form-group">
                <label for="startdate">Merchandise Name</label>
                    <input class="form-control" id="atitle" type="text" name="title">
                </div>
                <div class="form-group">
                <label for="startdate">Description</label>
                    <textarea class="form-control" id="acontent" type="text" name="content" rows="5" maxlength="100"> </textarea>
                </div>
                 <div class="form-group">     
                <label for="position">Add Image</label>      
                    <input class="form-control" type="file" id="userfile" name="userfile">
                </div> 
                <div class="form-group">
                <label for="startdate">Price</label>
                    <input class="form-control" id="aprice" type="number" name="price" step=".1">
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" id="create">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>           
            </div>
        </div>
    </div>
</div>   


<script>
$(document).ready(function(){
    anntbl();
   function anntbl(){
        $.ajax({
            type:'ajax',
            url: '<?=base_url()?>admin/merchtbl',
            method:'POST',
            success: function(data){
                $("#merchtable").html(data);
                // console.log(data);
            },
            error: function(){
            alert('No data');
            }
        });//end of ajax	 
   }
    $("#create").click(function(){
        if($('#atitle').val()==""){
            $('#atitle').attr('class', 'form form-control alert-danger');
            alert('Please fill the title');
        }  
        // else if($('#acontent').val()==""){
        //     $('#acontent').attr('class', 'form form-control alert-danger');
        //     alert('Please fill the content description');
        // }
        else if($('#aprice').val()==""){
            $('#acontent').attr('class', 'form form-control alert-danger');
            alert('Please fill the price');
        }
        else{
            $("#add_merch").submit();
        }     
    });

    $("#edit").click(function(){
        if($('#title').val()==""){
            $('#atitle').attr('class', 'form form-control alert-danger');
            alert('Please fill the title');
        }  
        // else if($('#content').val()==""){
        //     $('#content').attr('class', 'form form-control alert-danger');
        //     alert('Please fill the content description');
        // }
        else if($('#price').val()==""){
            $('#acontent').attr('class', 'form form-control alert-danger');
            alert('Please fill the price');
        }
        else{
            $("#edit_merch").submit();
        }     
    });

});

// function viewModal(title,content,start,end){
//     $("#title").val(title);
//     $("#content").val(content);
//     $("#title").val(title);
//     $("#title").val(title);
// }

</script>