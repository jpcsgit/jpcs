<div class="wrapper">
    <!-- Sidebar Constructor -->
    <div class="constructor">
<!-- <div class="container"> -->

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-3 mb-5 bg-white rounded" style="margin-top: 10px; overflow-y:scroll">
                <div class="card-body">
                    <h1 class="card-title display-4"><i class="material-icons" style="font-size:48px;">list</i>&nbsp;Members</h1>
                    <p class="card-text">Members database gives you quick access to manage members that let you safeguard organization's data, privacy, and decide how your information can make services work better for us.</p>
                    <hr>
                     <a href="javascript:void(0);" onclick="$('#importFrm').slideToggle();">Import Members</a>
                     <form action="<?=base_url()?>members/csvupload" method="post" enctype="multipart/form-data" id="importFrm">
                        <input type="file" name="file" />
                        <input type="submit" class="btn btn-primary" name="submit" value="IMPORT">
                    </form>
                    <a id="export" href="<?=base_url()?>members/export" class="btn btn-default">Export Members</a>
                    </br>
                    <button onclick="memberstbl();" class="btn btn-default" href="<?=base_url()?>/members">All</button>
                    <button onclick="memberstbl('local');" class="btn btn-default" href="<?=base_url()?>/members?type=local">Local</button>
                    <button onclick="memberstbl('national');" class="btn btn-default" style="background-color:orange;" href="<?=base_url()?>/members?type=national">National</button>
                    <br/><br/>
                    <div id="memberstbl">
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<div class="modal fade" style="height:600px;" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Member's Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Student Number: <h2 id="id"></h2></p>
                        <h2 id="fullname"></h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                            <strong>Course:</strong><p id="course"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                        <p><strong>Contact No.:</strong> <p id="contact"></p></br>
                        <strong>Email Address:</strong> <p id="email"></p><br/>
                            <strong>Facebook Profile: </strong><a id="link"><p id="fb_link"></p></a></br>
                            <strong>FEU Tech Email:</strong><p id="fit"></p></a></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>
            
            </div>
        </div>
    </div>
</div>  
<script>
$(document).ready(function(){
    memberstbl();
    $('#importFrm').hide();	   
});

function memberstbl(type){
    $.ajax({
        type:'ajax',
        url: '<?=base_url()?>members/memberstbl',
        data:{
            'type':type
        },
        method:'POST',
        success: function(data){
            $("#memberstbl").html(data);
            // console.log(data);
        },
        error: function(data){
        alert('No data');
        console.log(data);
        }
    });//end of ajax	 
}

function viewModal(id,fname,mname,lname,course,contact,email,fb,fit,member){
    $('#id').html(id);
    $('#fullname').html(lname+", "+fname+" "+mname);
    $('#course').html(course);
    $('#contact').html(contact);
    $('#email').html(email);
    $('#fb_link').html(fb);
    $('#link').attr("href",fb);
    $('#fit').html(fit);
    $('#member').html(member);
}
</script>