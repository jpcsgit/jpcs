<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow p-3 mb-5 bg-white rounded animated slideInUp" style="margin-top: 30px;">
                        <div class="card-body">
                            <h1 class="card-title display-4" ><i class="material-icons" style="font-size:48px;">create</i>&nbsp;Edit Member's Detail</h1>
                            <hr><br/>             
                            <form method="POST" action="<?=base_url()?>accounts/doedit">
                                <input type="hidden" name="studno" value="<?= $member->student_no ?>">
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="MemLName">Last Name: </label>
                                        <input type="text" class="form-control" value="<?= $member->lastname ?>" placeholder="Last Name" name="lname" required>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <label for="MemFName">First Name: </label>
                                        <input type="text" class="form-control" value="<?= $member->firstname ?>" placeholder="First Name" name="fname" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="MemMI">Middle Initial: </label>
                                        <input type="text" class="form-control" value="<?= $member->middlename ?>" placeholder="Middle Name" name="mname">
                                    </div>
                                </div>
                                <br/>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="MemAddress">Home Address: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">home</i></span>
                                            </div>
                                            <input type="text" value="<?= $member->address ?>" class="form-control" placeholder="Your Home Address" name="address" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="MemBirth">Birthdate: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">cake</i></span>
                                            </div>
                                            <input type="date" value="<?= date('Y-m-d', $member->birthday); ?>" class="form-control" name="birth" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="MemGender">Gender: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">wc</i></span>
                                            </div>
                                            <select class="form-control" name="gender" required>
                                                <option value="">...</option>
                                                <option value="Male" <?php if($member->gender == 'Male'){echo 'selected';}?>>Male</option>
                                                <option value="Female" <?php if($member->gender == 'Female'){echo 'selected';}?>>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="MemYearLvl">Year Level: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">calendar_today</i></span>
                                            </div>
                                            <select class="form-control" name="year" required>
                                                <option value="">...</option>
                                                <option value="1st" <?php if($member->year_level == '1st'){echo 'selected';}?>>1st</option>
                                                <option value="2nd" <?php if($member->year_level == '2nd'){echo 'selected';}?>>2nd</option>
                                                <option value="3rd" <?php if($member->year_level == '3rd'){echo 'selected';}?>>3rd</option>
                                                <option value="4th" <?php if($member->year_level == '4th'){echo 'selected';}?>>4th</option>
                                                <option value="5th" <?php if($member->year_level == '5th'){echo 'selected';}?>>5th</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="MemCourse">Course: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">school</i></span>
                                            </div>
                                            <select class="form-control" name="course" value="<?= $member->course ?>" required>
                                                <option value="">...</option>
                                                <option value="BSCE" <?php if($member->course == 'BSCE'){echo 'selected';}?>>BSCE</option>
                                                <option value="BSME" <?php if($member->course == 'BSME'){echo 'selected';}?>>BSME</option>
                                                <option value="BSEE" <?php if($member->course == 'BSEE'){echo 'selected';}?>>BSEE</option>
                                                <option value="BSECE" <?php if($member->course == 'BSECE'){echo 'selected';}?>>BSECE</option>
                                                <option value="BSCPE" <?php if($member->course == 'BSCPE'){echo 'selected';}?>>BSCPE</option>
                                                <option value="BSCSSE" <?php if($member->course == 'BSCSSE'){echo 'selected';}?>>BSCSSE</option>
                                                <option value="BSCSBA" <?php if($member->course == 'BSCSBA'){echo 'selected';}?>>BSCSBA</option>
                                                <option value="BSITDA" <?php if($member->course == 'BSITDA'){echo 'selected';}?>>BSITDA</option>
                                                <option value="BSITWMA" <?php if($member->course == 'BSITWMA'){echo 'selected';}?>>BSITWMA</option>
                                                <option value="BSITAGD" <?php if($member->course == 'BSITAGD'){echo 'selected';}?>>BSITAGD</option>
                                                <option value="BSITSMBA" <?php if($member->course == 'BSITSMBA'){echo 'selected';}?>>BSITSMBA</option>
                                                <option value="BSEMCDA" <?php if($member->course == 'BSEMCDA'){echo 'selected';}?>>BSEMCDA</option>
                                                <option value="BSEMCAGD" <?php if($member->course == 'BSEMCAGD'){echo 'selected';}?>>BSEMCAGD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="contact">Contact Number: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend">+</span>
                                            </div>
                                            <input type="tel" class="form-control" value="<?= $member->contact_no ?>" placeholder="Ex. 639XXXXXXXXX" name="contact" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email Address: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">email</i></span>
                                            </div>
                                            <input type="email" class="form-control" value="<?= $member->email ?>" placeholder="Ex. someone@example.com" name="email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="MemFB">Facebook Profile URL: </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroupPrepend"><i class="material-icons" style="font-size:18px;">person</i></span>
                                            </div>
                                            <input type="url" value="<?= $member->fb_link ?>" class="form-control" placeholder="Ex. https://www.facebook.com/example" value="https://www.facebook.com/" name="fb" required>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-inline">
                                    <button type="submit" name="btnSubmit" class="btn btn-success btn-lg btn-block">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

        </script>