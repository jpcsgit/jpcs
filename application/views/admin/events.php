<div class="wrapper">
    <!-- Sidebar Constructor -->
<div class="constructor">

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow p-3 mb-5 bg-white rounded" style="margin-top: 30px;">
                <div class="card-body col-md-12">
                    <h1 class="card-title display-4"><i class="material-icons" style="font-size:48px;">list</i>&nbsp; Events</h1>
                    <!-- <p class="card-text">Members database gives you quick access to manage members that let you safeguard organization's data, privacy, and decide how your information can make services work better for us.</p> -->
                    <hr>
                    <br/><br/>
                      <button type="button" class="btn btn-warning" style="float:right;"
                      data-toggle="modal" data-target="#addModal" data-backdrop="false">
                        Create Event</button>
                    <hr>
                    </br>
                    <div id="eventtbl">
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<div class="modal fade" style="height:600px;" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Event Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form" action="<?=base_url()?>admin/submit_edit_event" id="edit_event" method="post" enctype="multipart/form-data">
                <div id="sched">
                <input type="hidden" id="event_id" name="id">
                </div>
                <center>
                 <img id = "img" width="400">
                </center>
                <div class="form-group">         
                    <input class="form-control" type="file" id="userfile" name="userfile">
                </div>     
                <div class="form-group">
                <label for="startdate">Event Title</label>
                    <input class="form-control" id="title" type="text" name="title">
                </div>
                <div class="form-group">
                <label for="startdate">Description</label>
                    <textarea class="form-control" id="content" type="text" name="content" rows="5" maxlength="200"> </textarea>
                </div>
                <div class="form-group">
                <label for="startdate">Venue</label>
                    <input class="form-control" id="venue" type="text" name="venue">
                </div>
                <div class="form-group">
                <label for="startdate">Start Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="startdate"  min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="startdate">
                    <input class="form-horizontal" id="starttime" type="time" name="starttime">
                    </div>
                </div>
                <div class="form-group">
                <label for="startdate">End Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="enddate" min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="enddate">
                    <input class="form-horizontal" id="endtime" type="time" name="endtime">
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" id="edit" >Edit</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>           
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" style="height:600px;" id="addModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="viewModalLabel">Event Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <form class="form" action="<?=base_url()?>admin/add_event" id="add_event" method="post" enctype="multipart/form-data">
                <div class="form-group">
                <label for="startdate">Event Title</label>
                    <input class="form-control" id="atitle" type="text" name="title">
                </div>
                <div class="form-group">
                <label for="startdate">Description</label>
                    <textarea class="form-control" id="acontent" type="text" name="content" rows="5" maxlength="200"> </textarea>
                </div>
                <div class="form-group">
                <label for="startdate">Venue</label>
                    <input class="form-control" id="venue" type="text" name="venue">
                </div>
                 <div class="form-group">   
                <label for="startdate">Event Image</label>      
                    <input class="form-control" type="file" id="userfile" name="userfile">
                </div> 
                <div class="form-group">
                <label for="startdate">Start Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="astartdate"  min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="startdate">
                    <input class="form-horizontal" id="starttime" type="time" name="starttime">
                    </div>
                </div>
                <div class="form-group">
                <label for="startdate">End Date</label>
                    <div class="form-horizontal">
                    <input class="form-horizontal" id="aenddate" min="<?= date('Y-m-d',strtotime('now'))?>" type="date" name="enddate">
                    <input class="form-horizontal" id="endtime" type="time" name="endtime">
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-success" id="create">Create</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="material-icons" style="font-size:18px">close</i>&nbsp;Close</button>           
            </div>
        </div>
    </div>
</div>   


<script>
$(document).ready(function(){
    eventtbl();
   function eventtbl(){
        $.ajax({
            type:'ajax',
            url: '<?=base_url()?>admin/eventtbl',
            method:'POST',
            success: function(data){
                $("#eventtbl").html(data);
                // console.log(data);
            },
            error: function(){
            alert('No data');
            }
        });//end of ajax	 
   }
    $("#create").click(function(){
        if($('#atitle').val()==""){
            $('#atitle').attr('class', 'form form-control alert-danger');
            alert('Please fill the title');
        }  
        else if($('#acontent').val()==""){
            $('#acontent').attr('class', 'form form-control alert-danger');
            alert('Please fill the content description');
        }
        else if($('#astartdate').val().length == 0){
          $('#astartdate').attr('class', 'form alert-danger');
          alert('Start date is empty');
        }
        else if($('#aenddate').val().length == 0){
          $('#aenddate').attr('class', 'form alert-danger');
          alert('End date is empty');
        }
        else if($('#astartdate').val() > $('#aenddate').val()){
          $('#aenddate').attr('class', 'form alert-danger');
          alert('Invalid Schedule');
          console.log('invalid');
      }
        else{
            $("#add_event").submit();
        }     
    });

    $("#edit").click(function(){
        if($('#title').val()==""){
            $('#atitle').attr('class', 'form form-control alert-danger');
            alert('Please fill the title');
        }  
        else if($('#content').val()==""){
            $('#content').attr('class', 'form form-control alert-danger');
            alert('Please fill the content description');
        }
        else if($('#startdate').val().length == 0){
          $('#startdate').attr('class', 'form alert-danger');
          alert('Start date is empty');
        }
        else if($('#enddate').val().length == 0){
          $('#enddate').attr('class', 'form alert-danger');
          alert('End date is empty');
        }
        else if($('#startdate').val() > $('#enddate').val()){
          $('#enddate').attr('class', 'form alert-danger');
          alert('Invalid Schedule');
          console.log('invalid');
      }
        else{
            $("#edit_event").submit();
        }     
    });

});

// function viewModal(title,content,start,end){
//     $("#title").val(title);
//     $("#content").val(content);
//     $("#title").val(title);
//     $("#title").val(title);
// }

</script>