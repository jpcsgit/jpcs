 <table class="table table-hover table-responsive-md" id="membertable">
                        <!--<caption>Legends: Admin - Can View, Modify, and Delete Data to every features of this system. | Editor - Can View, Modify, and Delete Data to Members Table only. | Registration - Can Add Data to Members Table only.</caption>-->
    <thead class="thead-light">
        <tr>
            <th scope="col">Student Number</th>
            <th scope="col">Firstname</th>
            <th scope="col">Middlename</th>
            <th scope="col">Lastname</th>                                
            <th scope="col">Course</th>
            <th scope="col">Date Registered</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($members as $m):
        if(!$m->facebook_link){
            $fb_link = "";
        }
        else{
            $fb_link = prep_url($m->facebook_link);
        }
        ?>
            <tr>
                <th scope="row"><?= $m->student_number ?></th>
                <td><?= $m->fname ?></td>
                <td><?= $m->mname ?></td>
                <td><?= $m->lname ?></td>
                <td><?= $m->course ?></td>
                <td><?= date("Y-M-d H:i:s",strtotime($m->created_at)) ?></td>
                <td>
                    <button type="button" class="btn btn-primary" data-backdrop="false" 
                    data-toggle="modal" data-target="#viewModal"
                    onclick="viewModal('<?= $m->student_number ?>','<?= $m->fname?>','<?= $m->mname?>','<?= $m->lname?>','<?= $m->course ?>',
                    '<?= $m->contact_number ?>','<?= $m->personal_email?>','<?=$fb_link?>','<?= $m->feutech_email ?>','<?= $m->membership_type?>')">
                        <i class="material-icons" style="font-size:18px">visibility</i>&nbsp;View</button>
                </td>                                
            </tr>
                                    
    <?php endforeach; ?>
    </tbody>
</table>
<script>
$(document).ready( function () {
$('#membertable').DataTable({
    "order": [],
    "scrollY":"400px",
    "scrollCollapse": true,
});
} );

</script>