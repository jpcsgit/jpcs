<?php
//load the database configuration file

class csvupload {
    function upload($data){
    include 'dbConfig.php';

    // if(isset($_POST['importSubmit'])){
        
        //validate whether uploaded file is a csv file
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(!empty($data['file']['name']) && in_array($data['file']['type'],$csvMimes)){
            if(is_uploaded_file($data['file']['tmp_name'])){
                
                //open uploaded csv file with read only mode
                $csvFile = fopen($data['file']['tmp_name'], 'r');
                
                //skip first line
                fgetcsv($csvFile);
                
                //parse data from csv file line by line
                while(($line = fgetcsv($csvFile)) !== FALSE){
                    //check whether member already exists in database with same email
                    $prevQuery = "SELECT * FROM members";
                    $prevResult = $db->query($prevQuery);
                    if($prevResult->num_rows > 0){
                        //update member data
                        $db->query("UPDATE members SET name = '".$line[0]."', phone = '".$line[2]."', created = '".$line[3]."', modified = '".$line[3]."', status = '".$line[4]."' WHERE email = '".$line[1]."'");
                    }else{
                        //insert member data into database
                        $db->query("INSERT INTO members (student_number, fname, mname, lname, course, contact_number, cellular_provider,
                        personal_email, feutech_email, facebook_link, membership_type) 
                        VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[3]."','".$line[4]."',
                        '".$line[5]."','".$line[6]."','".$line[7]."','".$line[8]."','".$line[9]."','".$line[10]."')");
                    }
                }
                
                //close opened csv file
                fclose($csvFile);

                $qstring = '?status=succ';
            }else{
                $qstring = '?status=err';
            }
        }else{
            $qstring = '?status=invalid_file';
        }
        return $qstring;
    }
}
// }
?>