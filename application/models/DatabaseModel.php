<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DatabaseModel extends CI_Model {
    public function insert($table,$data){
        $q=$this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    public function insert_batch($table,$data){
        $q=$this->db->insert_batch($table,$data);
        // return $this->db->insert_batch();
    }

     public function update($table,$data,$where){ 
        if($where != NULL){       
        $this->db->where($where);
        }
        $q = $this->db->update($table, $data);
        return $q;
    }

    public function delete($table,$where){
        $q = $this->db->delete($table, $where);
        // return $q->affected_rows();
    }

     public function fetchAll($table,$where){
        if(!$where == null){
            $this->db->where($where);
        }
        $q = $this->db->get($table);
        return $q->result();
    }


    public function get_row($table,$where){        
        $this->db->where($where);
        $q = $this->db->get($table);

        return $q->row();
    }

    public function count($table,$where){
        if(!$where == null){
            $this->db->where($where);
        }
        $q = $this->db->get($table);
        return $q->num_rows();
    }

    public function sum($table,$where,$col){
        if(!$where == null){
            $this->db->where($where);
        }
        $this->db->select_sum($col);
        $q = $this->db->get($table);
        return $q->result();
    }

     public function fetchAllMembers($table,$where){
        if(!$where == null){
            $this->db->where($where);
        }
        $this->db->order_by('created_at', 'ASC');
        $q = $this->db->get($table);
        return $q->result();
    }

    public function fetchAllMembersArray($table,$where){
        if(!$where == null){
            $this->db->where($where);
        }
        $this->db->order_by('id', 'ASC');
        $q = $this->db->get($table);
        return $q->result_array();
    }

    public function fetchJCoin($table,$where){
        
    }
}
