<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model("DatabaseModel");           
    }
    
	public function index()
	{
		if($this->session->isAdmin != TRUE){
			// $this->load->view('admin/includes/header');
			// $this->load->view('admin/includes/navbar');
			$this->load->view('admin/login');
			$this->load->view('admin/includes/footer');		
			// print_r($this->session->userdata());	
		}
		else{
			redirect('admin/dashboard');
		}
	}

	public function dashboard(){
		if($this->session->isAdmin != TRUE){
			redirect('admin/index');			
		}
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$data = array(
			'userinfo'=>$account,
			'members'=>$this->DatabaseModel->count('members',NULL),
			'events'=>$this->DatabaseModel->count('tbl_event',NULL),
		);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account));
		$this->load->view('admin/dashboard',$data);
		$this->load->view('admin/includes/footer');
	}

	public function login(){
		$studnumber = $this->input->post('studnum');
		$password = sha1($this->input->post('password'));

		$where = array(
			'student_no'=>$studnumber,
			'password'=>$password,
		);
		$account = $this->DatabaseModel->get_row('admin',$where);
		if(!empty($account)){
			if($account->access=="Admin"){
				$session = array(
					'userId'=>$account->admin_id,
					'isAdmin'=>TRUE,
					'isLogin'=>TRUE,
				);
				$this->session->set_userdata($session);
				redirect('admin/index');
			}
		}
		else{
			redirect('admin/index');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('admin/index');		
	}

// =======================events crud====================================

	public function events(){
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account) );
		$this->load->view('admin/events');
		$this->load->view('admin/includes/footer');
	}
	public function eventtbl(){
		$data = array(
			'events'=>$this->DatabaseModel->fetchAll('tbl_event',array("event_status"=>1))
		);
		$this->load->view('admin/eventtbl',$data);
	}

	public function add_event(){
		// date_default_timezone_set('Asia/Manila');
		$data = array(
			'event_name'=>$this->input->post('title'),
			'event_description'=>$this->input->post('content'),
			'event_venue'=>$this->input->post('venue'),
			'start'=>strtotime($this->input->post('startdate')." ".$this->input->post('starttime')),
			'end'=>strtotime($this->input->post('enddate')." ".$this->input->post('endtime')),
			'event_status'=>1,
		);
		$event_id = $this->DatabaseModel->insert('tbl_event',$data);
		if(!empty($_FILES['userfile']['name'])){
			$config['upload_path']          = './images/events/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['max_size']             = 0;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['file_name']            = $event_id;
			$config['overwrite']            = TRUE;

			$this->load->library('upload',$config);
			
			// $this->upload->initialize($config);
			
			if (!$this->upload->do_upload('userfile'))
			{
				// echo $this->upload->display_errors()."<br>";
				echo "<script>alert('Image size is large')</script>";
			}
			else{
					$data2 = $this->upload->data();
					$data["event_img_path"]=$data2['file_name']; 
					$where["event_id"] = $merch_id;
					$this->DatabaseModel->update('tbl_event',$data,$where);
        	}
		}
		redirect('admin/events','refresh');
	}

	public function submit_edit_event(){
		$where["event_id"] = $this->input->post('id');
		$data = array(
			'event_name'=>$this->input->post('title'),
			'event_description'=>$this->input->post('content'),
			'event_venue'=>$this->input->post('venue'),
			'start'=>strtotime($this->input->post('startdate')." ".$this->input->post('starttime')),
			'end'=>strtotime($this->input->post('enddate')." ".$this->input->post('endtime')),
		);
		$fetch = $this->DatabaseModel->get_row('tbl_event',$where);
		if(!empty($_FILES['userfile']['name'])){
			$config['upload_path']          = './images/events/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['max_size']             = 0;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['file_name']            = $fetch->event_id;
			$config['overwrite']            = TRUE;

			$this->load->library('upload',$config);
			
			// $this->upload->initialize($config);
			
			@unlink("./images/events/".$fetch->event_img_path);
			
			if (!$this->upload->do_upload('userfile'))
			{
				// echo $this->upload->display_errors()."<br>";
				echo "<script>alert('Image size is large')</script>";
			}
			else{
					$data2 = $this->upload->data();
					$data["event_img_path"]=$data2['file_name']; 
        	}
		}
		$this->DatabaseModel->update('tbl_event',$data,$where);
		redirect('admin/events','refresh');
	}

	public function stop_event(){
		$where["event_id"] = $this->uri->segment(3);
		$data = array(
			'event_status'=>2,
		);
		$this->DatabaseModel->update('tbl_event',$data,$where);
		redirect('admin/events','refresh');
	}

	public function cancel_event(){
		$where["event_id"] = $this->uri->segment(3);
		$data = array(
			'event_status'=>3,
		);
		$this->DatabaseModel->update('tbl_event',$data,$where);
		redirect('admin/events');
	}

// =======================announcements crud====================================

	public function announcements(){	
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account) );
		$this->load->view('admin/announcements');
		$this->load->view('admin/includes/footer');
	}
	public function anntbl(){
		$data = array(
			'announcements'=>$this->DatabaseModel->fetchAll('announcements',array('ann_status'=>1))
		);
		$this->load->view('admin/announcementtbl',$data);
	}

	public function add_ann(){
		// date_default_timezone_set('Asia/Manila');
		$data = array(
			'ann_title'=>$this->input->post('title'),
			'ann_content'=>$this->input->post('content'),
			'ann_end_date'=>strtotime($this->input->post('enddate')." ".$this->input->post('endtime')),
			'ann_status'=>1,
		);
		
		$this->DatabaseModel->insert('announcements',$data);
		redirect('admin/announcements');
	}

	public function submit_edit_ann(){
		// date_default_timezone_set('Asia/Manila');
		$where["ann_id"] = $this->input->post('id');
		$data = array(
			'ann_title'=>$this->input->post('title'),
			'ann_content'=>$this->input->post('content'),
			'ann_end_date'=>strtotime($this->input->post('enddate')." ".$this->input->post('endtime')),
		);
		$this->DatabaseModel->update('announcements',$data,$where);
		redirect('admin/announcements');
	}

	public function remove_ann(){
		$where["ann_id"] = $this->uri->segment(3);
		$data = array(
			'ann_status'=>2,
		);
		$this->DatabaseModel->update('announcements',$data,$where);
		redirect('admin/announcements');
	}

	// ======================================= Mechandise ============================


	public function merchandise(){	
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account) );
		$this->load->view('admin/merchandise');
		$this->load->view('admin/includes/footer');
	}
	
	public function merchtbl(){
		$data = array(
			'merchandise'=>$this->DatabaseModel->fetchAll('tbl_merchandise',array('merch_status'=>1))
		);
		$this->load->view('admin/merchandisetbl',$data);
	}

	public function add_merch(){
		// date_default_timezone_set('Asia/Manila');
		$data = array(
			'merch_name'=>$this->input->post('title'),
			'merch_content'=>$this->input->post('content'),
			'merch_status'=>1,
			'merch_price'=>$this->input->post('price'),
		);
		$merch_id = $this->DatabaseModel->insert('tbl_merchandise',$data);
		if(!empty($_FILES['userfile']['name'])){
			$config['upload_path']          = './images/merchandise/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['max_size']             = 0;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['file_name']            = $merch_id;
			$config['overwrite']            = TRUE;

			$this->load->library('upload',$config);
			
			// $this->upload->initialize($config);
			
			if (!$this->upload->do_upload('userfile'))
			{
				// echo $this->upload->display_errors()."<br>";
				echo "<script>alert('Image size is large')</script>";
			}
			else{
					$data2 = $this->upload->data();
					$data["merch_img_path"]=$data2['file_name']; 
					$where["merch_id"] = $merch_id;
					$this->DatabaseModel->update('tbl_merchandise',$data,$where);
        	}
		}
		
		redirect('admin/merchandise','refresh');
	}

	public function submit_edit_merch(){
		// date_default_timezone_set('Asia/Manila');
		$where["merch_id"] = $this->input->post('id');
		$data = array(
			'merch_name'=>$this->input->post('title'),
			'merch_content'=>$this->input->post('content'),
			'merch_price'=>$this->input->post('price'),
		);
		$fetch = $this->DatabaseModel->get_row('tbl_merchandise',$where);
		if(!empty($_FILES['userfile']['name'])){
			$config['upload_path']          = './images/merchandise/';
			$config['allowed_types']        = 'jpg|png|jpeg';
			$config['max_size']             = 0;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['file_name']            = $fetch->merch_id;
			$config['overwrite']            = TRUE;

			$this->load->library('upload',$config);
			
			// $this->upload->initialize($config);
			
			@unlink("./images/merchandise/".$fetch->merch_img_path);
			
			if (!$this->upload->do_upload('userfile'))
			{
				// echo $this->upload->display_errors()."<br>";
				echo "<script>alert('Image size is large')</script>";
			}
			else{
					$data2 = $this->upload->data();
					$data["merch_img_path"]=$data2['file_name']; 
        	}
		}
		
		$this->DatabaseModel->update('tbl_merchandise',$data,$where);
		redirect('admin/merchandise','refresh');
	}

	public function remove_merch(){
		$where["merch_id"] = $this->uri->segment(3);
		$data = array(
			'merch_status'=>2,
		);
		$this->DatabaseModel->update('tbl_merchandise',$data,$where);
		redirect('admin/merchandise');
	}
}
