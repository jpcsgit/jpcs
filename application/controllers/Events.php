<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model("DatabaseModel");           
    }
    
	public function index(){	
		if($this->session->isUser != TRUE){					
			redirect('admin');
		}
				
		else{
			$where['acc_id']=$this->session->userId;
			$data = array(				
				'events'=>$this->DatabaseModel->fetchAll('tbl_event',array('event_status'=>1)),
			);
			$account = $this->DatabaseModel->get_row('accounts',$where);
			$this->load->view('home/includes/header',array('title'=>"Events"));
			$this->load->view('home/includes/navbar',array('admin'=>$account));	
			$this->load->view('home/events',$data);
			$this->load->view('home/includes/footer');
		}
			
	}

}
