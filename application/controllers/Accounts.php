<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model("DatabaseModel");           
    }
    
    
	public function index(){
		if($this->session->isAdmin != TRUE){
			redirect('admin/index');			
		}
		$data = array(
			'members'=>$this->DatabaseModel->fetchAll('accounts',NULL)
		);
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account));
		$this->load->view('admin/accounts',$data);
		$this->load->view('admin/includes/footer');
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
	}

	public function edit(){
		if($this->session->isAdmin != TRUE){
			redirect('admin/index');			
		}
		$where['student_no'] = $this->input->get('studnum');

		$data = array(
			'member'=>$this->DatabaseModel->get_row('accounts',$where)
		);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar');
		$this->load->view('admin/editaccounts',$data);
		$this->load->view('admin/includes/footer');
	}

	public function doEdit(){
		$where['student_no'] = $this->input->post('studno');
		$data = array(
			'firstname' => $this->input->post('fname'),
            'lastname' => $this->input->post('lname'),
            'middlename' => $this->input->post('mname'),
            'year_level' => $this->input->post('year'),
            'birthday' => strtotime($this->input->post('fname')),
            'address' => $this->input->post('address'),
            'gender' => $this->input->post('gender'),
            'course' => $this->input->post('course'),
            'contact_no' => $this->input->post('contact'),
            'email' => $this->input->post('email'),
            'fb_link' => $this->input->post('fb'),
		);
		$this->DatabaseModel->update('accounts',$data,$where);
		redirect('accounts/edit?studnum='.$where['student_no']);
	}

	public function delete(){

	}
}
