<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model("DatabaseModel");   
		$this->load->library('csv/csvupload');        
    }
    
    
	public function index(){
		if($this->session->isAdmin != TRUE){
			redirect('admin/index');			
		}		
		$where['admin_id']=$this->session->userId;
		$account = $this->DatabaseModel->get_row('admin',$where);
		$this->load->view('admin/includes/header');
		$this->load->view('admin/includes/navbar',array('admin'=>$account));
		$this->load->view('admin/members');
		$this->load->view('admin/includes/footer');
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
	}

	public function memberstbl(){
		$where['membership_type'] = $this->input->post('type');
		if($where['membership_type'] == NULL){
			$data = array(
				'members'=>$this->DatabaseModel->fetchAllMembers('members',NULL)
			);
		}
		else{
			$data = array(
				'members'=>$this->DatabaseModel->fetchAllMembers('members',$where)
			);
		}
		$this->load->view('admin/memberstbl',$data);
	}
	
	public function csvupload(){
		// $this->load->library('csv/csvupload');
		$data = $this->input->post('csv');
		$this->upload($data);
	}
	// public function delete(){

	// }
	function upload($data){
  		$conn = new mysqli("localhost", "root", "", "portal");
		if(isset($_POST["submit"]))
		{
		if($_FILES['file']['name'])
		{
			$filename = explode(".", $_FILES['file']['name']);
			if($filename[1] == 'csv')
				{
				$handle = fopen($_FILES['file']['tmp_name'], "r");
				while($line = fgetcsv($handle))
				{
					$query = "INSERT INTO members (student_number, fname, mname, lname, course, contact_number, cellular_provider,
					personal_email, feutech_email, facebook_link, membership_type) 
					VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."',
					'".$line[5]."','".$line[6]."','".$line[7]."','".$line[8]."','".$line[9]."','".$line[10]."')";
					// mysqli_query($connect, $query);
					if ($conn->query($query) === TRUE) {
						echo "New record created successfully";
					} else {
						echo "Error: " . $query . "<br>" . $conn->error;
					}
					echo "<pre>";
				print_r($line);
				echo "</pre>";
				}
				
				fclose($handle);
				echo "<script>alert('Import done');</script>";
				}
			}
		}
		redirect('members/');
    }

	function export(){
	  header('Content-Type: text/csv; charset=utf-8');  
      header('Content-Disposition: attachment; filename=data.csv');  
      $output = fopen("php://output", "w");  
      fputcsv($output, array('id', 'student_number', 'fname', 'mname', 'lname', 'course', 'contact_number', 'cellular_provider',
	  'personal_email', 'feutech_email', 'facebook_link', 'membership_type', 'created_at', 'updated_at')); 
	   $data = $this->DatabaseModel->fetchAllMembersArray('members',NULL);

	  foreach($data as $m){
		fputcsv($output, $m);
		// echo"<pre>";
		// print_r($m);
		// echo"</pre>";
	  }

	  fclose($output); 
	}
}
