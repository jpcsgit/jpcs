<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model("DatabaseModel");           
    }
    
	public function index(){	
		// $data = array(
		// 	'userinfo'=>$this
		// );
		if($this->session->isUser != TRUE){
					
			$this->login();
		}
				
		else{
			$where['acc_id']=$this->session->userId;
			$fetch = $this->DatabaseModel->get_row('accounts',$where);
			$data = array(				
				'announcements'=>$this->DatabaseModel->fetchAll('announcements',NULL),
				'events'=>$this->DatabaseModel->fetchAll('tbl_event',array('event_status'=>1)),
				'userinfo'=> $fetch,
				'points'=>$this->DatabaseModel->sum('tbl_points',array('student_number'=>$fetch->student_number),'points'),
			);
			$this->load->view('home/includes/header',array('title'=>"Home"));
			$this->load->view('home/includes/navbar');	
			$this->load->view('home/index2',$data);
			$this->load->view('home/includes/footer');

		}
			
	}

	public function login(){
		 $where = array(
            'student_number'=>$this->input->post('studnum'),
            'acc_password'=>sha1($this->input->post('password')),
        );
        $message['message'] = NULL;
		$account = $this->DatabaseModel->get_row('accounts',$where);
        if(isset($where['student_number']) && isset($where['acc_password'])){
			if(!empty($account)){
					$session = array(
						'userId'=>$account->acc_id,
						'isUser'=>TRUE,
						'isLogin'=>TRUE,
					);
					$this->session->set_userdata($session);
					print_r($account);
			}
			else{
				echo"<script>alert('Invalid student number or password')</script>";
			}
		}
		if($this->session->isUser != TRUE){
			$where = array();
			$this->load->view('home/login');
		}
		else{
			redirect('home/index');
		}			

	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('home/index');		
	}
}
